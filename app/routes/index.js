const { Router } = require('express');
const router = Router();
const controllers = require('../controllers');
const empresas = require('../controllers/empresas');
const productos = require('../controllers/productos');
const categorias = require('../controllers/categorias');
// Rutas :D 
router.get('/', controllers.GetInicio);
router.get('/empresas', empresas.listarEmpresas);
router.post('/empresas/add', empresas.agregarEmpresa);
router.get('/empresas/add', empresas.badUse);// Leer Readme.md
router.get('/empresas/search', empresas.search);// Leer Readme.md
router.delete('/empresas' , empresas.deleteEmpresa);//borrar empresa por id
router.get('/productos' , productos.getProductos);
router.post('/productos' , productos.agregarProducto);
module.exports = router;
