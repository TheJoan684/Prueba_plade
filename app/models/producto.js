const { Model } = require('objection');

class Producto extends Model {
	static get tableName(){
        return 'productos';
    }

    static get totalProductosPorEmpresas(){ //este metodo me contabiliza la cantidad de productos que pertenencen a una empresa id
        return 'total';
    }
    static get empresaIdColumn(){ //esto es para saber a que empresa pertenece cada producto
    	return 'id_empresa';
    }
} 
module.exports = Producto;
